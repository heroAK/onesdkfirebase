# OneSDKFireBase

[![CI Status](https://img.shields.io/travis/wangtotang/OneSDKFireBase.svg?style=flat)](https://travis-ci.org/wangtotang/OneSDKFireBase)
[![Version](https://img.shields.io/cocoapods/v/OneSDKFireBase.svg?style=flat)](https://cocoapods.org/pods/OneSDKFireBase)
[![License](https://img.shields.io/cocoapods/l/OneSDKFireBase.svg?style=flat)](https://cocoapods.org/pods/OneSDKFireBase)
[![Platform](https://img.shields.io/cocoapods/p/OneSDKFireBase.svg?style=flat)](https://cocoapods.org/pods/OneSDKFireBase)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

OneSDKFireBase is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'OneSDKFireBase'
```

## Author

akhero, 2387825290@qq.com

## License

OneSDKFireBase is available under the MIT license. See the LICENSE file for more info.
