//
//  main.m
//  OneSDKFireBase
//
//  Created by wangtotang on 05/22/2019.
//  Copyright (c) 2019 wangtotang. All rights reserved.
//

@import UIKit;
#import "OneSDKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OneSDKAppDelegate class]));
    }
}
