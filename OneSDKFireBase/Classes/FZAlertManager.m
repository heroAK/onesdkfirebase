//
//  FZAlertManager.m
//  FaceToFace
//
//  Created by lioi on 16/12/12.
//  Copyright © 2016年 F2F. All rights reserved.
//

#import "FZAlertManager.h"

static FZAlertManager *manager = nil;

@interface FZAlertManager(){
    UIAlertController *_alert;
    UITapGestureRecognizer *_tap;
}

@property (strong, nonatomic) UIViewController *currentVc;


@end

@implementation FZAlertManager
+ (instancetype)manager{
    
    manager = [[FZAlertManager alloc]init];
    return manager;
}

- (UIViewController *)showAlertOfController:(UIViewController *)controller Title:(NSString *)title Message:(NSString *)message SureStr:(NSString *)sureStr CancleStr:(NSString *)cancleStr TouchSure:(void (^)())sure{

    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:sureStr? sureStr:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        if (sure) {
            sure();
        }
    }];
    UIAlertAction*action2 = [UIAlertAction
                             actionWithTitle:cancleStr?cancleStr:@"取消" style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction *action){
                                 
                             }];
    
    [alert addAction:action1];
    
    [alert addAction:action2];
    
    [controller presentViewController:alert animated:YES completion:^{
        
    }];
    
    _alert = alert;
    
    // 增加点击事件
    UIWindow *alertWindow = (UIWindow *)[UIApplication sharedApplication].windows.lastObject;
    alertWindow.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideAlert)];
    _tap = tap;
    
    return alert;
}

- (void)hideAlert
{
    UIWindow *alertWindow = (UIWindow *)[UIApplication sharedApplication].windows.lastObject;
    [alertWindow removeGestureRecognizer:_tap];
    [_alert dismissViewControllerAnimated:YES completion:nil];
}


- (UIViewController *)showAlertOfController:(UIViewController *)controller Title:(NSString *)title Message:(NSString *)message SureStr:(NSString *)sureStr CancleStr:(NSString *)cancleStr TouchSure:(void (^)())sure TouchCancle:(void (^)())cancle{

    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];

    
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:sureStr? sureStr:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        if (sure) {
            sure();
        }
    }];
    UIAlertAction*action2 = [UIAlertAction
                             actionWithTitle:cancleStr?cancleStr:@"取消" style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction *action){
                                 if (cancle) {
                                     cancle();
                                 }
                                 
                                 
                             }];
    
    [alert addAction:action1];
    
    [alert addAction:action2];
    
    [controller presentViewController:alert animated:YES completion:^{
        
    }];
    return alert;
}

- (UIViewController *)showAlertOfController:(UIViewController *)controller Title:(NSString *)title Message:(NSString *)message SureStr:(NSString *)sureStr{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *action1 = [UIAlertAction actionWithTitle:sureStr? sureStr:@"知道了" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            }];
    [alert addAction:action1];
    
    
    
    [controller presentViewController:alert animated:YES completion:^{
        
    }];
    return alert;

}

//一个按钮,只有知道了
- (UIViewController *)showAlertOfController:(UIViewController *)controller Title:(NSString *)title Message:(NSString *)message SureStr:(NSString *)sureStr TouchSure:(void (^)())sure{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:sureStr? sureStr:@"知道了" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        if (sure) {
            sure();
        }
    }];
    [alert addAction:action1];
    
    
    
    [controller presentViewController:alert animated:YES completion:^{
        
    }];
    return alert;
}

//一个按钮,只有知道了
- (UIViewController *)hb_showAlertOfController:(UIViewController *)controller Title:(NSString *)title textAlignment:(NSTextAlignment)textAlignment Message:(NSString *)message SureStr:(NSString *)sureStr TouchSure:(void (^)())sure{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIView *subView1 = alert.view.subviews[0];
    UIView *subView2 = subView1.subviews[0];
    UIView *subView3 = subView2.subviews[0];
    UIView *subView4 = subView3.subviews[0];
    UIView *subView5 = subView4.subviews[0];
    
    //设置内容的对齐方式
    UILabel * messageLab = subView5.subviews[2];
    if ([message.class isEqual:[UILabel class]]) {
        messageLab.textAlignment = textAlignment;
    }else
    {
        UILabel * messageLab = subView5.subviews[1];
        messageLab.textAlignment = textAlignment;
    }
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:sureStr? sureStr:@"知道了" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        if (sure) {
            sure();
        }
    }];
    [alert addAction:action1];
    
    
    
    [controller presentViewController:alert animated:YES completion:^{
        
    }];
    return alert;
}

@end
