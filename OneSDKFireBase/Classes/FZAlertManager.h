//
//  FZAlertManager.h
//  FaceToFace
//
//  Created by lioi on 16/12/12.
//  Copyright © 2016年 F2F. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FZAlertManager : NSObject

+ (instancetype)manager;

//没有cancle回调
- (UIViewController *)showAlertOfController:(UIViewController *)controller Title:(NSString *)title Message:(NSString *)message SureStr:(NSString *)sureStr CancleStr:(NSString *)cancleStr TouchSure:(void (^)())sure;

//有cancle回调
- (UIViewController *)showAlertOfController:(UIViewController *)controller Title:(NSString *)title Message:(NSString *)message SureStr:(NSString *)sureStr CancleStr:(NSString *)cancleStr TouchSure:(void (^)())sure TouchCancle:(void (^)())cancle;

//一个按钮,只有知道了
- (UIViewController *)showAlertOfController:(UIViewController *)controller Title:(NSString *)title Message:(NSString *)message SureStr:(NSString *)sureStr;

//一个按钮,只有知道了
- (UIViewController *)showAlertOfController:(UIViewController *)controller Title:(NSString *)title Message:(NSString *)message SureStr:(NSString *)sureStr TouchSure:(void (^)())sure;
/*
 * 一个按钮,只有知道了
 * textAlignment message对齐方式
 */
- (UIViewController *)hb_showAlertOfController:(UIViewController *)controller Title:(NSString *)title textAlignment:(NSTextAlignment)textAlignment Message:(NSString *)message SureStr:(NSString *)sureStr TouchSure:(void (^)())sure;
@end
