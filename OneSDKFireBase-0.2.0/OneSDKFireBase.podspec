Pod::Spec.new do |s|
  s.name = "OneSDKFireBase"
  s.version = "0.2.0"
  s.summary = "Firebase Cloud Message of OneSDKFireBase."
  s.license = {"type"=>"MIT", "file"=>"LICENSE"}
  s.authors = {"AK"=>"2387825290@qq.com"}
  s.homepage = "https://gitlab.com/heroAK/onesdkfirebase"
  s.description = "TODO: Firebase Cloud Message, Add long description of the pod here."
  s.source = { :path => '.' }

  s.ios.deployment_target    = '8.0'
  s.ios.vendored_framework   = 'ios/OneSDKFireBase.framework'
end
